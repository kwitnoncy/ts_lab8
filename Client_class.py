from socket import *
import time
import tkinter

class Client():
    liczby = (0, 0)
    OperaC = None
    IdentY = 0
    Wynik = 0

    def __init__(self, addr):
        self.Wynik_str = "Wynik: None"
        """
            Ustawienia okna
        """
        self.window = tkinter.Tk(className="ts_lab8")
        self.input1 = tkinter.Entry(self.window)
        self.input2 = tkinter.Entry(self.window)
        self.sol_label = tkinter.Label(self.window, text=self.Wynik_str)
        self.sol_label.grid(row=7)

        ####
        soc_set = socket(AF_INET, SOCK_STREAM)
        soc_set.connect((addr, 8000))
        data = soc_set.recv(1000)
        self.IdentY = int((((str(data).split("\'")[1]).split(">>"))[2]).split('^')[0])
        print(self.IdentY)
        soc_set.close()
        ####

        """
            Ustawienia gniazda
        """
        self.socket = socket(AF_INET, SOCK_STREAM)
        port = int(8000 + self.IdentY)
        self.socket.connect((addr, port))

        print(self.IdentY)
        tkinter.Label(self.window, text=("ID: " + str(self.IdentY))).grid(row=0)

        self.run()

    def __del__(self):
        self.disconnect()
        print("quited")

    def run(self):
        tkinter.Label(self.window, text="liczba1").grid(row=1)
        tkinter.Label(self.window, text="liczba2").grid(row=2)
        self.input1.grid(row=1, column=1)
        self.input2.grid(row=2, column=1)

        tkinter.Button(self.window, text='Quit', command=self.disconnect).grid(row=4, column=0, pady=4)
        tkinter.Button(self.window, text='Show', command=self.show_entry_fields).grid(row=4, column=1, pady=4)
        tkinter.Button(self.window, text='op1 - \"-\"', command=self.set_opt1).grid(row=5, column=0, pady=1, padx=50)
        tkinter.Button(self.window, text='op2 - \"+\"', command=self.set_opt2).grid(row=6, column=0, pady=4, padx=50)
        tkinter.Button(self.window, text='op3 - \"*\"', command=self.set_opt3).grid(row=5, column=1, pady=4, padx=50)
        tkinter.Button(self.window, text='op4 - \"\\\"', command=self.set_opt4).grid(row=6, column=1, pady=4, padx=50)

        self.window.mainloop()

    def show_entry_fields(self):
        self.get_nums()

        print("liczba 1: ")
        if self.liczby[0] is None:
            print("None")
        else:
            print(self.liczby[0])
        print("liczba 2: ")
        if self.liczby[1] is None:
            print("None")
        else:
            print(self.liczby[1])

    def set_opt1(self):
        if self.get_nums():
            self.OperaC = 'op1'
            print(self.liczby)
            self.send_n_recv()
        else:
            print("zle liczby")

    def set_opt2(self):
        if self.get_nums():
            self.OperaC = 'op2'
            print(self.liczby)
            self.send_n_recv()
        else:
            print("zle liczby")

    def set_opt3(self):
        if self.get_nums():
            self.OperaC = 'op3'
            print(self.liczby)
            self.send_n_recv()
        else:
            print("zle liczby")

    def set_opt4(self):
        if self.get_nums():
            self.OperaC = 'op4'
            print(self.liczby)
            self.send_n_recv()
        else:
            print("zle liczby")

    def send_n_recv(self):
        print("send_recv liczby: ", self.liczby)
        mess_to_send = str("OperaC>>" + self.OperaC + "^StatuS>>^Identy>>" + str(self.IdentY) + "^liczba1>>" + str(
            self.liczby[0]) + "^liczba2>>" + str(self.liczby[1]) + "^SolutI>>None^Time>>" + str(time.time()) + "^").encode()
        self.socket.send(mess_to_send)

        data = self.socket.recv(1024)

        data = str(data).split("'")[1]
        data = data.split("^")

        for i in range(0, len(data)):
            data[i] = data[i].split(">>")

        self.Wynik = data[3][1]
        self.sol_label.config(text=self.set_solution_str())


    def disconnect(self):
        self.OperaC = 'DisC'
        self.liczby = 0, 0
        mess_to_send = str("OperaC>>" + self.OperaC + "^Identy>>" + str(self.IdentY) + "^liczba1>>" + str(
            self.liczby[0]) + "^liczba2>>" + str(self.liczby[1]) + "^SolutI>>None^Time>>" + str(time.time()) + "^").encode()
        self.socket.send(mess_to_send)
        self.window.quit()

    def set_solution_str(self):
        return "Wynik: " + str(self.Wynik)

    def get_nums(self):
        num1 = self.input1.get()
        x = num1
        try:
            x = int(x)
        except ValueError:
            try:
                x = int(x, 2)
            except ValueError:
                try:
                    x = int(x, 16)
                except ValueError:
                    x = None

        num2 = self.input2.get()
        y = num2
        try:
            y = int(y)
        except ValueError:
            try:
                y = int(y, 2)
            except ValueError:
                try:
                    y = int(y, 16)
                except ValueError:
                    y = None

        self.liczby = x, y

        if x is None or y is None:
            return False
        else:
            return True