from socket import *
from random import randint
import sys
import threading
import numpy as np
import time
import datetime


class Thread_sock(threading.Thread):
    create_new = False
    available_ports = np.zeros(1000)

    def __init__(self):
        print("nowy watek")
        threading.Thread.__init__(self)

        self.running = False
        self.ID = randint(0, 1000)

        while self.available_ports[self.ID] == 1:
            self.ID = randint(0, 1000)

        self.available_ports[self.ID] = 1

        ####
        try:
            soc_set = socket(AF_INET, SOCK_STREAM)
            soc_set.bind(('', 8000))
            soc_set.listen(5)
        except KeyboardInterrupt:
            print("guzik")
            self.__del__()
        else:
            c, a = soc_set.accept()

            print("Polaczenie wstepne: ", a)

            ID_mess = ("OperaC>>SET^IdentY>>" + str(self.ID) + "^").encode()
            c.send(ID_mess)

            soc_set.close()
            ####

            self.s = socket(AF_INET, SOCK_STREAM)
            self.s.bind(('', int(8000 + self.ID)))
            self.s.listen(5)
            print(self.ID)

            self.liczby = [0, 0]
            self.OperaC = None
            self.IdentY = 0
            self.SolutioN = 0

            self.create_new = False

            self.start()

    def __del__(self):
        print("(" + str(self.ID) + "):  koniec")

    def run(self):
        client, addr = self.s.accept()
        self.running = True
        self.create_new = True

        print("(" + str(self.ID) + "):  Polaczenie własciwe: ", addr)


        while self.running:
            data = client.recv(1024)
            data = (str(data).split("'"))[1]  # pozbycie sie b'
            data = (str(data).split("^"))  # podzial na poszczegolne pola

            for i in range(0, len(data)):
                data[i] = data[i].split(">>")

            print("hehe", data)

            self.OperaC = data[0][1]

            if self.OperaC == 'op1':
                self.liczby[0] = int(data[3][1])
                self.liczby[1] = int(data[4][1])
                print("(" + str(self.ID) + "):  op1: " + str(self.liczby[0]) + " - " + str(self.liczby[1]))
                self.OperaC = "SolutI"
                self.SolutioN = self.liczby[0] - self.liczby[1]

                mess_to_send = str("OperaC>>" + self.OperaC + "^StatuS>>" + "^IdentY>>" + str(self.ID) + "^SolutI>>" + str(self.SolutioN) + "^Time>>" + str(time.time()) + '^').encode()
                client.send(mess_to_send)

            elif self.OperaC == 'op2':
                self.liczby[0] = int(data[3][1])
                self.liczby[1] = int(data[4][1])
                print("(" + str(self.ID) + "):  op2: " + str(self.liczby[0]) + " + " + str(self.liczby[1]))
                self.OperaC = "SolutI"
                self.SolutioN = self.liczby[0] + self.liczby[1]

                mess_to_send = str("OperaC>>" + self.OperaC + "^StatuS>>" + "^IdentY>>" + str(self.ID) + "^SolutI>>" + str(self.SolutioN) + "^Time>>" + str(time.time()) + '^').encode()

                client.send(mess_to_send)

            elif self.OperaC == 'op3':
                self.liczby[0] = int(data[3][1])
                self.liczby[1] = int(data[4][1])
                print("(" + str(self.ID) + "):  op3: " + str(self.liczby[0]) + " * " + str(self.liczby[1]))
                self.OperaC = "SolutI"
                self.SolutioN = self.liczby[0] * self.liczby[1]

                mess_to_send = str("OperaC>>" + self.OperaC + "^StatuS>>" + "^IdentY>>" + str(self.ID) + "^SolutI>>" + str(self.SolutioN) + "^Time>>" + str(time.time()) + '^').encode()

                client.send(mess_to_send)

            elif self.OperaC == 'op4':
                self.liczby[0] = int(data[3][1])
                self.liczby[1] = int(data[4][1])
                print("(" + str(self.ID) + "):  op4: " + str(self.liczby[0]) + " / " + str(self.liczby[1]))
                self.OperaC = "SolutI"
                self.SolutioN = self.liczby[0] / self.liczby[1]

                mess_to_send = str("OperaC>>" + self.OperaC + "^StatuS>>^IdentY>>" + str(self.ID) + "^SolutI>>" + str(self.SolutioN) + "^Time>>" + str(time.time()) + '^').encode()

                client.send(mess_to_send)

            elif self.OperaC == 'DisC':
                print("(" + str(self.ID) + "):  DISC")
                self.s.close()
                client.close()
                self.running = False

    def get_port(self):
        return self.port